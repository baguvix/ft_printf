/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 17:50:08 by rnarbo            #+#    #+#             */
/*   Updated: 2018/12/07 23:19:56 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	start;
	size_t	end;
	size_t	len;
	char	*res;

	if (!s)
		return (0);
	start = 0;
	while (s[start] == ' ' || s[start] == '\n' || s[start] == '\t')
		++start;
	len = ft_strlen(s);
	if (len == start || len == 0)
	{
		if ((res = (char *)malloc(1)) == 0)
			return (0);
		res[0] = 0;
		return (res);
	}
	end = len - 1;
	while (s[end] == ' ' || s[end] == '\n' || s[end] == '\t' || s[end] == 0)
		--end;
	res = ft_strsub(s, start, end - start + 1);
	return (res);
}
