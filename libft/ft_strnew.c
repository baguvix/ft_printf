/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 21:55:10 by rnarbo            #+#    #+#             */
/*   Updated: 2019/01/23 19:21:55 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	size_t	i;
	char	*res;

	i = 0;
	if ((res = (char *)malloc((unsigned long long)size + 1)) == 0)
		return (0);
	while (i < size + 1)
		res[i++] = 0;
	return (res);
}
