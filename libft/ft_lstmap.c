/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/06 21:50:10 by rnarbo            #+#    #+#             */
/*   Updated: 2019/01/23 18:57:27 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *head;
	t_list *tmp;
	t_list *del_tmp;

	if (!lst || !f)
		return (0);
	head = f(lst);
	tmp = head;
	lst = lst->next;
	while (lst)
	{
		if ((tmp->next = f(lst)) == 0)
		{
			while (head != tmp)
			{
				del_tmp = head->next;
				free(head);
				head = del_tmp;
			}
			return (0);
		}
		lst = lst->next;
		tmp = tmp->next;
	}
	return (head);
}
