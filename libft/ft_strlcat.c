/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 18:03:30 by rnarbo            #+#    #+#             */
/*   Updated: 2018/12/07 21:57:08 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *s1, const char *s2, size_t size)
{
	size_t	i;
	long	j;
	long	s;
	size_t	res;

	i = 0;
	while (i < size && s1[i])
		i++;
	j = 0;
	res = (size < ft_strlen(s1) ? size : ft_strlen(s1)) + ft_strlen(s2);
	if (i == size)
		return (res);
	s = size - 1 - i;
	while (j < s && s2[j])
	{
		s1[i] = s2[j];
		i++;
		j++;
	}
	s1[i] = 0;
	return (res);
}
