/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 21:49:47 by rnarbo            #+#    #+#             */
/*   Updated: 2018/11/24 21:53:27 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	void	*res;
	size_t	i;

	if ((res = malloc(size)) == 0)
		return (0);
	i = 0;
	while (i < size)
		((unsigned char *)res)[i++] = 0;
	return (res);
}
