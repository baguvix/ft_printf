/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/12 17:56:17 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/09 13:22:43 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstfind(t_list *head, void *content, size_t content_size)
{
	if (!head)
		return (0);
	while (head && ft_memcmp(head->content, content, content_size) != 0)
		head = head->next;
	return (head);
}
