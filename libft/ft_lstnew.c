/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 22:13:21 by rnarbo            #+#    #+#             */
/*   Updated: 2018/12/07 22:13:24 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*res;

	if ((res = (t_list *)malloc(sizeof(t_list))) == 0)
		return (0);
	res->next = 0;
	if (content == 0)
	{
		res->content = 0;
		res->content_size = 0;
		return (res);
	}
	if ((res->content = malloc(content_size)) == 0)
		return (0);
	ft_memcpy(res->content, content, content_size);
	res->content_size = content_size;
	return (res);
}
