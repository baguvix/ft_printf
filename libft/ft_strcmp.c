/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 19:19:48 by rnarbo            #+#    #+#             */
/*   Updated: 2019/01/02 17:58:41 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	unsigned char *s1u;
	unsigned char *s2u;

	s1u = (unsigned char *)s1;
	s2u = (unsigned char *)s2;
	while (*s1u && *s2u && *s1u == *s2u)
	{
		s1u++;
		s2u++;
	}
	return (*s1u - *s2u);
}
