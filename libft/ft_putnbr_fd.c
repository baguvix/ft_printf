/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/07 22:15:30 by rnarbo            #+#    #+#             */
/*   Updated: 2018/12/07 22:15:31 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int nbr, int fd)
{
	long	i;
	int		sign;

	sign = 1;
	if (nbr < 0)
	{
		sign = -1;
		ft_putchar_fd('-', fd);
	}
	i = 10;
	while (nbr / i != 0)
		i *= 10;
	i /= 10;
	while (i != 0)
	{
		ft_putchar_fd(sign * (nbr / i) + '0', fd);
		nbr = nbr % i;
		i /= 10;
	}
}
