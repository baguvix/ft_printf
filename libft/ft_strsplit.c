/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 18:28:30 by rnarbo            #+#    #+#             */
/*   Updated: 2018/12/07 23:33:04 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	words_count(char const *s, char c)
{
	size_t	res;
	int		wf;

	res = 0;
	wf = 0;
	while (*s)
	{
		if (*s != c)
		{
			if (wf == 0)
				++res;
			wf = 1;
		}
		else
			wf = 0;
		s++;
	}
	return (res);
}

static size_t	word_len(char const *s, char c)
{
	int		res;

	res = 0;
	while (s[res] && s[res] != c)
		++res;
	return (res);
}

static long		free_arr(char ***res, size_t i)
{
	while (i > 0)
		free(*res[--i]);
	free(*res);
	return (0);
}

char			**ft_strsplit(char const *s, char c)
{
	int		was_letter;
	size_t	i;
	size_t	j;
	char	**res;
	size_t	count;

	if ((i = 0) == 0 && !s)
		return (0);
	count = words_count(s, c);
	if ((res = (char **)malloc((count + 1) * sizeof(char *))) == 0)
		return (0);
	j = 0;
	was_letter = 0;
	while (i < count)
	{
		while (s[j] == c)
			j++;
		if ((res[i] = (char *)malloc(word_len(s + j, c) + 1)) == 0)
			return ((char **)free_arr(&res, i));
		ft_strncpy(res[i], s + j, word_len(s + j, c));
		res[i++][word_len(s + j, c)] = 0;
		j += word_len(s + j, c);
	}
	res[i] = 0;
	return (res);
}
