/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 16:08:37 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/19 23:03:31 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

int		nofd(unsigned long long a, short base)
{
	int	count;

	count = 1;
	a /= base;
	while (a && ++count)
		a /= base;
	return (count);
}

void	del_with_free(void *content, size_t size)
{
	size = 0;
	if (((t_spec *)content)->type == '0')
		free(((t_spec *)content)->value.vp);
	free(content);
}

int		cmp(t_list *l1, t_list *l2)
{
	return (((t_spec *)l1->content)->num - ((t_spec *)l2->content)->num);
}

void	del(void *lst, size_t content_size)
{
	lst += 0;
	content_size += 0;
}

int		unitoa(char *dest, wchar_t c)
{
	int		len;
	int		i;

	if ((len = 1) == 1 && c < 0x80)
	{
		dest[0] = c;
		return (len);
	}
	if (c < 0x800)
		len = 2;
	else
		len = c < 0x10000 ? 3 : 4;
	i = 0;
	while (c)
	{
		dest[len - 1 - i++] = (c & 0x3f) | 0x80;
		c >>= 6;
	}
	dest[0] = (dest[0] & (0xff >> (len + 1))) | (0xf0 << (4 - len));
	return (len);
}
