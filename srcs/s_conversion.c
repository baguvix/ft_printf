/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_conversion.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 14:01:05 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/19 23:04:04 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "libft.h"
#include "utils.h"

static size_t	wclen(wchar_t c)
{
	if (c < 0x80)
		return (1);
	if (c < 0x800)
		return (2);
	return (c < 0x10000 ? 3 : 4);
}

static size_t	wcsbnlen(wchar_t *str, size_t size)
{
	size_t res;

	res = 0;
	while (*str)
	{
		if (res + wclen(*str) > size)
			break ;
		res += wclen(*str);
		str++;
	}
	return (res);
}

ssize_t			bs_conv(t_spec *spec, char **rtu)
{
	size_t str_size;
	size_t str_len;
	size_t i;
	size_t j;

	if (spec->value.vp == 0 && (spec->value.vp = L"(null)") == 0)
		return (-1);
	str_len = wcsbnlen(spec->value.vp, spec->prec);
	if (spec->prec < (int)str_len && spec->prec != -1)
		str_len = spec->prec;
	str_size = (size_t)spec->width > str_len ? spec->width : str_len;
	if (spec->align_left)
		spec->filling = ' ';
	if ((*rtu = (char *)malloc(str_size + 1)) == 0)
		return (-1);
	ft_memset(*rtu, spec->filling, str_size);
	i = spec->align_left ? 0 : str_size - str_len;
	str_len = spec->align_left ? str_len : str_size;
	j = 0;
	while (i < str_len)
		if (wclen(((wchar_t *)spec->value.vp)[j]) + i <= str_len)
			i += unitoa(*rtu + i, ((wchar_t *)spec->value.vp)[j++]);
		else
			break ;
	return (str_size);
}

ssize_t			s_conv(t_spec *spec, char **rtu)
{
	size_t str_size;
	size_t str_len;
	size_t i;
	size_t j;

	if (spec->value.vp == 0 && (spec->value.vp = "(null)") == 0)
		return (-1);
	str_len = ft_strlen(spec->value.vp);
	if (spec->prec < (int)str_len && spec->prec != -1)
		str_len = spec->prec;
	str_size = (size_t)spec->width > str_len ? spec->width : str_len;
	if (spec->align_left)
		spec->filling = ' ';
	if ((*rtu = (char *)malloc(str_size + 1)) == 0)
		return (-1);
	ft_memset(*rtu, spec->filling, str_size);
	i = spec->align_left ? 0 : str_size - str_len;
	str_len = spec->align_left ? str_len : str_size;
	j = 0;
	while (i < str_len)
		(*rtu)[i++] = ((char *)spec->value.vp)[j++];
	return (str_size);
}
