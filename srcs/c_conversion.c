/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_conversion.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 13:42:46 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/19 23:01:31 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>
#include <stdlib.h>
#include "libft.h"
#include "utils.h"

ssize_t	c_conv(t_spec *spec, char **rtu)
{
	size_t	len;
	char	uc[4];
	int		unilen;

	if (spec->lenght.l || spec->type == 'C')
		unilen = unitoa(uc, spec->value.ll);
	else
	{
		uc[0] = (char)spec->value.ll;
		unilen = 1;
	}
	len = (spec->width > unilen ? spec->width : unilen);
	*rtu = (char *)malloc(len);
	if (*rtu == NULL)
		return (-1);
	ft_memset(*rtu, spec->filling, len);
	if (spec->align_left)
		ft_memcpy(*rtu, uc, unilen);
	else
		ft_memcpy(*rtu + len - unilen, uc, unilen);
	return (len);
}
